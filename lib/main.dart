import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_simon/ui/home/home_page.dart';

void main() {
  // Permet de lancer l'application en mode desktop sur Linux
  // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override
  // BLE : pratique pour tester sans lancer d'émulateur
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(new SimonApp());
}

class SimonApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simon',
      debugShowCheckedModeBanner: false,
      theme: _buildThemeData(),
      initialRoute: HomePage.routeName,
      routes: <String, WidgetBuilder>{
        HomePage.routeName: (BuildContext context) => HomePage(),
      },
    );
  }

  ThemeData _buildThemeData() {
    return ThemeData(
        brightness: Brightness.dark,
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          TargetPlatform.fuchsia: CupertinoPageTransitionsBuilder()
        }));
  }
}
