import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class _PageScore extends MaterialPageRoute<Null> {
  _PageScore()
      : super(builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.orange,
              leading: Icon(Icons.brightness_auto),
              title: Text("SIMON"),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.equalizer),
                  focusColor: Colors.red,
                  onPressed: () {},
                ),
                IconButton(
                  icon: Icon(Icons.build),
                  onPressed: () {},
                ),
                IconButton(
                  icon: Icon(Icons.menu),
                  onPressed: () {},
                )
              ],
            ),
            body: Center(
              child: Text("Hey, you're on the high score page."),
            ),
          );
        });
}
