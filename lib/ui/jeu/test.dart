import 'dart:async';
import 'dart:math';
import 'globales.dart' as globals;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'PageScore.dart' as pagescore;

class SimonWidget extends StatefulWidget {
  @override
  _SimonWidgetState createState() => _SimonWidgetState();
}

class _SimonWidgetState extends State<SimonWidget> {
  Color couleurDeFond1 = Colors.indigoAccent[700];
  Color couleurDeFond2 = Colors.amber[600];
  Color couleurDeFond3 = Colors.red[900];
  Color couleurDeFond4 = Colors.green[600];
  var score = globals.niveau.toString();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        leading: Icon(Icons.brightness_auto),
        title: Text("SIMON"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.equalizer),
            focusColor: Colors.red,
            onPressed: () {
              //Navigator.push(context, route);
            },
          ),
          IconButton(
            icon: Icon(Icons.build),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          )
        ],
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                SizedBox(
                  height: 90,
                  child: RaisedButton(
                    onPressed: clicBouton1,
                    color: couleurDeFond1,
                    shape: RoundedRectangleBorder(borderRadius: const BorderRadius.only(topLeft: Radius.circular(90.0))),
                  ),
                ),
                SizedBox(
                    height: 90,
                    child: RaisedButton(
                      onPressed: clicBouton3,
                      color: couleurDeFond3,
                      shape: RoundedRectangleBorder(borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(90.0))),
                    )),
                Spacer(
                  flex: 1,
                ),
                Container(
                  child: Text('SCORE :'),
                )
              ],
            ),
            Column(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                SizedBox(
                    height: 90,
                    child: RaisedButton(
                      onPressed: clicBouton2,
                      color: couleurDeFond2,
                      shape: RoundedRectangleBorder(borderRadius: const BorderRadius.only(topRight: Radius.circular(90.0))),
                    )),
                SizedBox(
                  height: 90,
                  child: RaisedButton(
                    onPressed: clicBouton4,
                    color: couleurDeFond4,
                    shape: RoundedRectangleBorder(borderRadius: const BorderRadius.only(bottomRight: Radius.circular(90.0))),
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Container(
                  child: Text(score),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  var aleatoire = new Random();
  List<int> couleurAnimation = [];

  addNumToSeed() {
    globals.seed.add(aleatoire.nextInt(3) + 1);
    print(globals.seed);
  }

  clignoter() async {
    await Future.delayed(const Duration(milliseconds: 100));
    for (var x = 0; x < globals.seed.length; x++) {
      await Future.delayed(const Duration(milliseconds: 300));
      setState(() {
        score = globals.niveau.toString();
        if (globals.seed[x] == 1) {
          couleurDeFond1 = Colors.lightBlueAccent[400];
        }
        if (globals.seed[x] == 2) {
          couleurDeFond2 = Colors.yellow[500];
        }
        if (globals.seed[x] == 3) {
          couleurDeFond3 = Colors.redAccent;
        }
        if (globals.seed[x] == 4) {
          couleurDeFond4 = Colors.greenAccent[700];
        }
      });
      await Future.delayed(const Duration(milliseconds: 900));
      setState(() {
        if (globals.seed[x] == 1) {
          couleurDeFond1 = Colors.indigoAccent[700];
        }
        if (globals.seed[x] == 2) {
          couleurDeFond2 = Colors.amber[600];
        }
        if (globals.seed[x] == 3) {
          couleurDeFond3 = Colors.red[900];
        }
        if (globals.seed[x] == 4) {
          couleurDeFond4 = Colors.green[600];
        }
      });
    }
  }

  clicBouton1() {
    if (!globals.animationEnCours) {
      globals.reponse = 1;
      verification();
    }
  }

  clicBouton2() {
    if (!globals.animationEnCours) {
      globals.reponse = 2;
      verification();
    }
  }

  clicBouton3() {
    if (!globals.animationEnCours) {
      globals.reponse = 3;
      verification();
    }
  }

  clicBouton4() {
    if (!globals.animationEnCours) {
      globals.reponse = 4;
      verification();
    }
  }

  verification() {
    if (globals.reponse != globals.seed[globals.etape]) {
      print('perdu');
    } else {
      if (globals.niveau == globals.etape) {
        print('bravo');
        globals.etape = 0;
        globals.niveau += 1;
        addNumToSeed();
        animationCouleur();
      } else {
        globals.etape += 1;
      }
    }
  }

  animationCouleur() {
    for (var i in globals.seed) {
      couleurAnimation.add(i);
      print('i =$i');
      globals.animationEnCours = true;
      clignoter();
    }
    globals.animationEnCours = false;
  }
}
