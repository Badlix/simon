import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_simon/ui/jeu/test.dart';

class HomePage extends StatelessWidget {
  static const routeName = 'home';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(color: Colors.black, child: SimonWidget()),
    );
  }
}
